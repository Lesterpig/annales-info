Correction FUS 2013
===================

Exercice 1
----------

1- Un inode

2- Un numéro d'inode

3- Lien Symbolique

4- Le contenu est `..` soit une taille de deux octets en ASCII

5- Je met pas la date, on peut pas savoir.

```
1951 drwcr-c--- 2  lepl teach 4096    .
1952 -rw-r----- 1  lepl teach 10      fich0
1953 -rw-r----- 3  lepl teach 16      fich1
1953 -rw-r----- 3  lepl teach 16      lnFich2
1956 lrwxrwxrwx 2  lepl teach 2       lnLns
1957 lrwxrwxrwx 1  lepl teach 5       lnsFich1
```

Exercice 2
----------

1- Elle détermine les dépendance de tous les fichiers se terminant en `.c`

2- 

```makefile
redrum.o: redrum.c redrum.h
	$(CC) -c redrum.c
```

3- Elle tente de compiler un fichier `.h` (première dépendance). Ca ne plantera pas, mais ça va faire n'importe quoi (création d'un fichier .h.gch).

4- Plantage, car gardy et torrance ont tous deux une fonction main

5- `$(CC) -o $@ $^`

6- Va correctement compiler le fichier grady. Toutes les dépendances sont en effet satisfaites (testé).

7- Au début du fichier make :

```makefile
default:
	make grady
	make torrance
```

8-

```makefile
clean:
	rm *.o grady torrence
```

9- Stanley Kubrick ;)

Exercice 3
----------

1- Les lignes contenant un E et/ou un I sont sélectionnées.

2- Ligne 1

3- `/[EI]/`, ou `/^.*[EI].*$/` s'il faut vérifier la ligne.

4- Tout sauf la dernière colonne. `.*` capture la chaîne la plus grande possible.

5- `/^(.*?);.*$/` ajouter un `?` afin de capturer la chaîne la plus courte possible.

6- Nombre d'arguments (index du dernier élément du tableau + 1)

7- Nom du script appelé

8- `($nbarg == 2) || die "Usage : $0 input output";`

9- Affectation des variables $entree et $sortie en fonction des paramètres

10- Ligne 14 : remplacer || et &&. Il ne faut tuer le script QUE si le fichier $sortie existe

11- Lecture de la première ligne

14- Une table de hash à une dimension

17- Une table de hash à deux dimensions

Le script final : (perl v5.18.2)

```perl
#!/usr/bin/perl

$nbarg = $#ARGV + 1;
($nbarg == 2) || die "Usage : $0 input output";
($entree, $sortie) = @ARGV;
(-e $entree) || die "Bad input";
(-e $sortie) && die "Bad output";

open(ENTREE, "<$entree");
$ligne = <ENTREE>;

@listeEntreprise = split(/;/,$ligne); # Question 12
splice(@listeEntreprise, 0, 4, ());   # Si vous trouvez en une ligne je suis preneur!

# Question 15

%departements = ("EII" => 0, "INFO" => 0, "SRC" => 0);

# Question 17

%etudiants = ();

# Question 20

%EtudCreneau = ();
%EntrepriseCreneau = ();
$maxCreneau = 4;

while ($ligne = <ENTREE>) {

	# Question 13

	@table = split(/;/,$ligne);
	
	# Question 18
	 
	$dep = "EII";

	if($table[2] == 1) {
		$dep = "INFO";
	}
	if($table[3] == 1) {
		$dep = "SRC";
	}

	$departements{$dep}++;


	$etudiants{$table[0]}{"DEPARTEMENT"} = $dep;
	$etudiants{$table[0]}{"ENTRETIENS"}  = 0;

	# Question 19
	
	print $table[0] . " : ";
	
	for($k = 0; $k <= $#listeEntreprise; $k++) {

		if($table[4+$k] == 1 || $table[4+$k] == 2) {
			print $listeEntreprise[$k] . ", ";
			remplirHoraire($listeEntreprise[$k], $table[0]);
		}

	}

	print "\n";

}

# Question 16

print "\nETUDIANTS :\n---------------\n";

while(($cle,$valeur) = each(%departements)) {
	print "$cle : $valeur etudiants\n";
}

print "\n";

while($nom = each(%etudiants)) {

	%details = %{$etudiants{$nom}};
	print $nom . " (" . $details{"DEPARTEMENT"} . ") : " . $details{"ENTRETIENS"} . " entretiens\n";

}


close ENTREE;

sub remplirHoraire($entreprise, $etudiant) {

	my ($entreprise, $etudiant) = @_; # Passage en variables locales

	# Question 21
	# Initialisation des créneaux vides.

	if(!exists($EtudCreneau{$etudiant})) {
		for($i = 1; $i <= $maxCreneau; $i++) {
			$EtudCreneau{$etudiant}{$i} = 0;
		}
	}

	if(!exists($EntrepriseCreneau{$entreprise})) {
		for($i = 1; $i <= $maxCreneau; $i++) {
			$EntrepriseCreneau{$entreprise}{$i} = "";
		}
	}

	for($i = 1; $i <= $maxCreneau; $i++) {
		if($EtudCreneau{$etudiant}{$i} == 0 && $EntrepriseCreneau{$entreprise}{$i} eq "") {
			$EtudCreneau{$etudiant}{$i} = 1;
			$EntrepriseCreneau{$entreprise}{$i} = $etudiant;
			return;
		}
	}

	print "!! $etudiant ne peut pas rencontrer $entreprise";

}
```

Contributeurs
-------------

- [Lesterpig](http://www.lesterpig.com)
- Lucas