#!/usr/bin/perl

$nbarg = $#ARGV + 1;
($nbarg == 2) || die "Usage : $0 input output";
($entree, $sortie) = @ARGV;
(-e $entree) || die "Bad input";
(-e $sortie) && die "Bad output";

open(ENTREE, "<$entree");
$ligne = <ENTREE>;

@listeEntreprise = split(/;/,$ligne); # Question 12
splice(@listeEntreprise, 0, 4, ());   # Si vous trouvez en une ligne je suis preneur!

# Question 15

%departements = ("EII" => 0, "INFO" => 0, "SRC" => 0);

# Question 17

%etudiants = ();

# Question 20

%EtudCreneau = ();
%EntrepriseCreneau = ();
$maxCreneau = 4;

while ($ligne = <ENTREE>) {

	# Question 13

	@table = split(/;/,$ligne);
	
	# Question 18
	 
	$dep = "EII";

	if($table[2] == 1) {
		$dep = "INFO";
	}
	if($table[3] == 1) {
		$dep = "SRC";
	}

	$departements{$dep}++;


	$etudiants{$table[0]}{"DEPARTEMENT"} = $dep;
	$etudiants{$table[0]}{"ENTRETIENS"}  = 0;

	# Question 19
	
	print $table[0] . " : ";
	
	for($k = 0; $k <= $#listeEntreprise; $k++) {

		if($table[4+$k] == 1 || $table[4+$k] == 2) {
			print $listeEntreprise[$k] . ", ";
			remplirHoraire($listeEntreprise[$k], $table[0]);
		}

	}

	print "\n";

}

# Question 16

print "\nETUDIANTS :\n---------------\n";

while(($cle,$valeur) = each(%departements)) {
	print "$cle : $valeur etudiants\n";
}

print "\n";

while($nom = each(%etudiants)) {

	%details = %{$etudiants{$nom}};
	print $nom . " (" . $details{"DEPARTEMENT"} . ") : " . $details{"ENTRETIENS"} . " entretiens\n";

}


close ENTREE;

sub remplirHoraire($entreprise, $etudiant) {

	my ($entreprise, $etudiant) = @_; # Passage en variables locales

	# Question 21
	# Initialisation des créneaux vides.

	if(!exists($EtudCreneau{$etudiant})) {
		for($i = 1; $i <= $maxCreneau; $i++) {
			$EtudCreneau{$etudiant}{$i} = 0;
		}
	}

	if(!exists($EntrepriseCreneau{$entreprise})) {
		for($i = 1; $i <= $maxCreneau; $i++) {
			$EntrepriseCreneau{$entreprise}{$i} = "";
		}
	}

	for($i = 1; $i <= $maxCreneau; $i++) {
		if($EtudCreneau{$etudiant}{$i} == 0 && $EntrepriseCreneau{$entreprise}{$i} eq "") {
			$EtudCreneau{$etudiant}{$i} = 1;
			$EntrepriseCreneau{$entreprise}{$i} = $etudiant;
			return;
		}
	}

	print "!! $etudiant ne peut pas rencontrer $entreprise";

}