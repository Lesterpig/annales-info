Correction FUS 2011
===================

Exercice 1
----------

1- `/home-reseau/lepl` : la commande cd amène en effet dans ce repertoire et pwd affiche le répertoire courant. En effet quand on ne passe aucun argument à la commande cd (ligne 1), elle déplace l'utilisateur dans son HOME.

2- Une erreur. On ne peut pas écrire dans le dossier `explosif`.

3- `ls -lia tnt`
  * On veut le contenu de tnt et pas tnt/boum
  * On enlève l'argument -d qui ne permet pas d'afficher le contenu du dossier mais son nom uniquement.

4- `chmod g+r mine`

5- `ln mine mine2`

6- `mkdir newDir`

7- `8955 -rwxr----- 2 lepl teacher 50 (date actuelle) bombe`

8- `8955 -rwxr----- 2 lepl teacher 50 (date actuelle) tnt/grenade`

9- `ln explosif badabouuuum`
On ne peut pas faire de lien hard sur un répertoire.


Exercice 2
----------

1- Liste les dépendances des fichiers

2- `pr1: pr1.o f1.o f2.o`

3- `$(CC) $^ -o $@`

4- `$(CC) -c $<`

5- Elle compile le fichier `f1.c` uniquement (première cible)

6- `f1.o` `pr1`

7- `f2.o` `pr1.o` `pr1` ET `f1.o` car `f1.h` et `f2.h` ont même numéro d'inode

Exercice 3
----------

1- Lignes contenant un `I` ou un `N` ou un `D` (22 lignes sur 24)

2- 23 et 9 

3- `/[IND]/`

4- `Combourg,35`

5- Ajouter un `?` après la troisième étoile

6- `/^\d+\s*(DATE)\s+([\w\s]*\w)\s*$/`

7- 

```perl
($#ARGV == 0) || die "blabla";
($fic) = @ARGV;
(-e $fic) || die "blabla";
```

8- Méthode 2 pour éviter les doublons d'expression régulière

9- `$refPLAC = ["^\d+\s*(PLAC)\s*(.*?),.*$", 5, \&traitGEN];`

10- `$etat = 0;`

11- Nombre d'éléments du tableau des états accessibles depuis l'état courant.

12- `$etat = ${automate{$etat}[$i][1]}`

13- Définition des variables locales

14- `$ptStruct->{$balise} = $valeur;`

15- 

```perl
sub traitINDI {
	my ($etat, $ref, $balise, $ptStruct) = @_;
	$ptStruct->{$balise} = $ref;
}
```

16-

```perl
sub traitNAME {
	my ($etat, $balise, $nom, $ptStruct) = @_;
	$nom =~ s/\//\ /g;
	$ptStruct->{$balise} = $nom;
}
```

17- L'état 5

18- Avant `last`

19- 

```perl
print $ptStruct->{"EVT"} . ": " .  $ptStruct->{"NAME"} . ", " . $ptStruct->{"DATE"} . ", " . $ptStruct->{"PLAC"} . "\n";`
```

*(je simplifie en mettant directement les noms des évènements)*

Contributeurs
-------------

- [Lesterpig](http://www.lesterpig.com)
- Laurent
- Quentin