Correction BDD 2014
===================

**AVERTISSEMENT**

Munissez vous d'un bon fauteuil et d'une boîte d'aspirine avant de commencer la lecture de ce document. Il est très fortement recommandé de fermer toute fenêtre et de ranger tout objet coupant afin de limiter les risques de blessure. Bonne lecture !

Exercice 1
----------

#### Question 1 :

```
A->B d'où AC->BC
comme A->C on a A->AC
par transitivité A->AC->BC d'où A->BC
```

#### Question 2 :

```
On remplace A par "Prénom", B par "Sexe", C par "Id client"
On a donc les relations suivantes :

Prénom    -> Sexe
Id client -> Sexe

Prénom -> Id client ?
Facile de faire une base avec un seul prénom pour des id clients différents.
La relation est donc fausse.
```

Exercice 2
----------

#### Question 3 :

Méthode -> https://www.youtube.com/watch?v=IiEAfRoHIH8

On a :

```
A+ = ABCD
B+ = BC
C+ = C
D+ = D
```

```
A  -> BC : passage en forme normale
B  -> C
A  -> B
AB -> C
AC -> D
```

```
A  -> B
A  -> C
B  -> C
AB -> C : A+ = [A,B,C,D] et B+ = [B,C] on peut donc enlever le B
AC -> D
```

```
A  -> B
A  -> C
B  -> C
AC -> D : A+ = [A,B,C,D] et C+ = [C] on peut donc enlever le C
```

```
A -> B
A -> C : par transitivité, A -> B -> C on peut donc enlever A -> C
B -> C
A -> D
```

Résultat :

```
A -> B
B -> C
A -> D

A+ = ABCD [OK]
B+ = BC   [OK]
C+ = C    [OK]
D+ = D    [OK]
```

#### Question 4 :

`A` semble être la seule clé possible pour déterminer tous les autres éléments.

Exercice 3
----------

#### Question 5 :

```
a) MD -> V
b) - (pas possibilité de mettre de limites numériques)
c) P  -> NpC
d) C  -> VNa
e) PM -> ECa
f) - (le champ organisateur n'existe pas)
```

#### Question 6 :

On a PMD -> VNpCNaECa. Cela semble être la seule clé : ni P, ni M, ni D ne sont du côté droit des D.F. Impossible de créer une nouvelle clé ne le contenant pas.

#### Question 7 :

On a P -> NpC. Comme P fait partie de la clé, on est automatiquement PAS en 2NF.

D'où 1NF car attributs mono-valués.

#### Question 8 :

Calculons la converture minimale !

```
MD -> V  : M+ = [M] et D+ = [D] on ne peut rien enlever
P  -> Np
P  -> C
C  -> V
C  -> Na
PM -> E  : P+ = [P,Np] et M+ = [M] on ne peut rien enlever
PM -> Ca : idem
```

On décompose selon `MD -> V`

```
DECOMPOSITION 1
[M,D,P,Np,C,Na,E,Ca] clé = PMD
P  -> Np
P  -> C
C  -> Na
PM -> E
PM -> Ca
---------------------------
DECOMPOSITION 2 : BCNF OK
[M,D,V] clé = MD
MD -> V
```

On décompose 1 selon `P -> Np`

```
DECOMPOSITION 1.1
[M,D,P,C,Na,E,Ca] clé = PMD
P  -> C
C  -> Na
PM -> E
PM -> Ca
---------------------------
DECOMPOSITION 1.2 : BCNF OK
[P,Np] clé = P
P  -> Np
---------------------------
DECOMPOSITION 2 : BCNF OK
[M,D,V] clé = MD
MD -> V
```

On décompose 1.1 selon `P -> C`

```
DECOMPOSITION 1.1.1
[M,D,P,Na,E,Ca] clé = PMDNa
PM -> E
PM -> Ca
---------------------------
DECOMPOSITION 1.1.2 : BCNF OK
[P,C] clé = P
P -> C
---------------------------
DECOMPOSITION 1.2 : BCNF OK
[P,Np] clé = P
P  -> Np
---------------------------
DECOMPOSITION 2 : BCNF OK
[M,D,V] clé = MD
MD -> V
```

On doit encore décomposer 1.1.1 pour virer D et Na de la clé. On décompose 1.1.1 selon `PM -> E`.

```
DECOMPOSITION 1.1.1.1
[M,D,P,Na,Ca] clé = PMDNa
PM -> Ca
---------------------------
DECOMPOSITION 1.1.1.2 : BCNF OK
[P,M,E] clé = PM
PM -> E
---------------------------
DECOMPOSITION 1.1.2 : BCNF OK
[P,C] clé = P
P -> C
---------------------------
DECOMPOSITION 1.2 : BCNF OK
[P,Np] clé = P
P  -> Np
---------------------------
DECOMPOSITION 2 : BCNF OK
[M,D,V] clé = MD
MD -> V
```

Enfin, on décompose 1.1.1.1 selon `PM -> Ca`.

```
DECOMPOSITION 1.1.1.1.1 : BCNF OK
[M,D,P,Na] clé = PMDNa
---------------------------
DECOMPOSITION 1.1.1.1.2 : BCNF OK
[P,M,Ca] clé = PM
PM -> Ca
---------------------------
DECOMPOSITION 1.1.1.2 : BCNF OK
[P,M,E] clé = PM
PM -> E
---------------------------
DECOMPOSITION 1.1.2 : BCNF OK
[P,C] clé = P
P -> C
---------------------------
DECOMPOSITION 1.2 : BCNF OK
[P,Np] clé = P
P  -> Np
---------------------------
DECOMPOSITION 2 : BCNF OK
[M,D,V] clé = MD
MD -> V
```

**Conclusion : la décomposition choisie donne [MDPNa, PMCa, PME, PC, PNp, MDV]** (oklm).

#### Question 9

On reprend la couverture minimale. On a la décomposition suivante :

```
MDV
PNp
PC
CV
CNa
PME
PMCa
```

Cependant, **aucune règle ne contient la clé PMD**. On doit donc ajouter une relation qui contient la clé.

**Conclusion : la décomposition choisie donne [MDV, PNp, PC, CV, CNa, PME, PMCa, *PMD*]**

#### Question 10

```
BNCF : ne préserve pas toutes les dépendances (il en manque 2)
3NF  : préserve les dépendances (par définition)
```

Validité de BNCF : *(là, c'est un peu à l'aveugle)*

On joint en utilisant les D.F. préservées par la décomposition.

```
MDV <M> PME <P> PC <P> PNp <PM> PMCa <PM> MDPNa [OK]
```

Validité de 3NF : forcément ok, on conserve toutes les D.F.

#### Question 11

La décomposition ne conserve que ces dépendances :

```
P  -> Np
P  -> C
```

Par conséquent, on ne peut joindre que par P. Or MNaV ne contient pas P. Donc non valide (à vérifier...).

Exercice 4
----------

#### Question 12

Je propose des arbres encore plus grands pour cette année...

**Arbre original**

```
                                         51
                                         ||
           11             30             ||             66            78            99               120
                                         ||
02 07 -- -- | 12 15 22 -- | 36 41 -- --  ||  53 54 57 63 | 68 69 71 76 | 79 84 93 -- | 100 103 112 -- | 130 140 150 160
```

**Ajout de 99** : On cherche la bonne feuille, puis on l'ajoute. Ici, il y a de la place donc aucun problème.

```
                                         51 >------------------->-------------------+
                                         ||                                         V
           11             30             ||             66            78            99 >------+      120
                                         ||                                                   V
02 07 -- -- | 12 15 22 -- | 36 41 -- --  ||  53 54 57 63 | 68 69 71 76 | 79 84 93 -- | 99 100 103 112 | 130 140 150 160
```

#### Question 13

**Ajout de 55** : La feuille contenant 55 est pleine, de même que son parent. On va devoir remonter à la racine.

Nouvel arbre (la partie < 51 n'est pas affichée) :

```
    51                                           78
    ||             55            66              ||             99               120
... ||                                           ||
    || 53 54 -- -- | 55 57 63 -- | 68 69 71 76   ||  79 84 93 -- | 100 103 112 -- | 130 140 150 160
```

Exercice 5
----------

#### Question 14

Un parcours séquentiel implique la lecture de la **totalité** des pages. On aura donc pour les deux requêtes 4000 E/S.

#### Question 15

On a au maximum 430 E/S lors de la première requête. En effet, avec beaucoup de malchance, les documents seront tous éparpillés dans les différentes pages. Grâce à l'index, on ne consulte **que** les documents intéressants.

#### Question 16

Pour la deuxième requête, l'index nous permet d'obtenir directement, et dans l'ordre, les documents. On va donc lire `ceil(1989/85)` pages dans le pire des cas, soit 24 E/S.

Exercice 6
----------

*Testé sous MySql 14.14*

#### Question 17

```sql
GRANT SELECT ON ADR TO Joe;
```

#### Question 18

```sql
CREATE VIEW RennesCustomers AS
  SELECT C.CNOM,C.CPRENOM FROM CLIENT AS C, ADR AS A
  WHERE C.AID = A.AID
  AND   A.VILLE = "Rennes";

GRANT SELECT,UPDATE ON RennesCustomers TO Nancy;
```

#### Question 19

```sql
CREATE VIEW CityListing AS
  SELECT A.VILLE,count(*) FROM CLIENT AS C, ADR AS A
  WHERE C.AID = A.AID
  GROUP BY A.VILLE;

GRANT SELECT ON CityListing TO Julie;
```

#### Question 20

```sql
CREATE VIEW WTFQuestion AS
  SELECT A.NR FROM ADR AS A
  WHERE A.VILLE IN (SELECT DISTINCT(LA.VILLE) FROM CLIENT AS LC, ADR AS LA
                  WHERE LC.AID = LA.AID
                    AND LC.CNOM = "Skywalker"
                    AND LC.CPRENOM = "Luke");

GRANT UPDATE ON WTFQuestion TO Julie,Joe;
```

Note : sur MySql, les UPDATE ne marcheront pas (sous requête donc vue non modifiable). Si vous trouvez mieux je suis preneur !