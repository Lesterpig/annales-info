#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void) {
    char mess4[61] = "EXO :";
    char menu1[][15] = {{" Menu : "}, {" Choix 1 "}, {" Choix 2 "}, {" Fin"}};
    int i;

    printf("%s (%d car)\n", mess4, (int) strlen(mess4));
    for(i = 0; i < strlen(menu1[0]); i++) {
        if(islower(menu1[0][i])) {
            menu1[0][i] = toupper(menu1[0][i]);
        }
    }
    strcpy(mess4,menu1[0]);
    printf("%s (%d car)\n", mess4, (int) strlen(mess4));

    //printf("Sizeof menu1 : %d\nSizeof menu1[0] : %d\n", (int) sizeof(menu1), (int) sizeof(menu1[0]));

    for(i = 1; i < sizeof(menu1)/sizeof(menu1[0]); i++) { // i = 1; i < 60/15; i++
        strcat(mess4, menu1[i]);
    }
    printf("%s\n", mess4);

    return 0;
}