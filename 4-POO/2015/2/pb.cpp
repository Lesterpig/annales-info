#include <iostream>
#include <vector>
using namespace std;

class Image {
  public:
    string nom;
    Image(string n) : nom(n) {}
};

class Doc {
  public:
    Doc(string n, string c) : nom(n), contenu(c) {}
    virtual void imprime() {
      cout << nom << endl << contenu << endl;
    }
    virtual string type() {
      return "doc";
    }

  protected:
    string nom;
    string contenu;
};

class Pdf : public Doc {
  public:
    Pdf(string n, string c) : Doc(n, 'P' + c) {}
    virtual string type() {
      return "pdf";
    }
};

class Word : public Doc {
  public:
    Word(string n, string c, Image* i) : Doc(n, 'W' + c), im(i) {}
    Pdf* exportPDF() {
      return new Pdf(nom, contenu.substr(1, contenu.size()) + im->nom);
    }
    virtual void imprime() {
      exportPDF()->imprime();
    }
    virtual string type() {
      return "word";
    }

  protected:
    Image* im;
};

class Repertoire {
  public:
    Repertoire() : documents() {}
    void ajoute(Doc* d) {
      documents.push_back(d);
    }
    void imprime() {
      vector<Doc*>::iterator iter;
      for (iter = documents.begin(); iter != documents.end(); iter++) {
        (*iter)->imprime();
        cout << "---" << endl;
      }
    }
    Repertoire recopie() {
      Repertoire* r = new Repertoire();
      vector<Doc*>::iterator iter;
      for (iter = documents.begin(); iter != documents.end(); iter++) {
        r->ajoute(*iter);
      }
      return *r;
    }
    Repertoire conversionPDF() {
      Repertoire* r = new Repertoire();
      vector<Doc*>::iterator iter;
      for (iter = documents.begin(); iter != documents.end(); iter++) {
        Doc* d = *iter;
        if (d->type() == "word") {
          d = ((Word*) d)->exportPDF();
        }
        r->ajoute(d);
      }
      return *r;
    }
    void imprimeType() {
      vector<Doc*>::iterator iter;
      for (iter = documents.begin(); iter != documents.end(); iter++) {
        (*iter)->imprime();
        cout << (*iter)->type() << endl;
        cout << "---" << endl;
      }
    }

  private:
    vector<Doc*> documents;
};


int main() {
  Image* image1 = new Image("image1");
  Word* w1 = new Word("toto", "textDeW1", image1);
  Pdf* p1 = new Pdf("tutu", "textDeP1");
  Repertoire r1;
  r1.ajoute(w1);
  r1.ajoute(p1);
  cout << "imp r1" << endl;
  r1.imprime();

  Repertoire r2;
  cout << "== conversion== " << endl;
  r2 = r1.conversionPDF();
  cout << "imp r1" << endl;
  r1.imprimeType();
  cout << "imp r2" << endl;
  r2.imprimeType();

  cout << "== recopie == " << endl;

  r2 = r1.recopie();
  cout << "imp r1" << endl;
  r1.imprimeType();
  cout << "imp r2" << endl;
  r2.imprimeType();

  delete image1;
  return 0;
}

/*
 *  imp r1
 *  toto
 *  PtextDeW1image1
 *  ---
 *  tutu
 *  PtextDeP1
 *  ---
 *  == conversion== 
 *  imp r1
 *  toto
 *  PtextDeW1image1
 *  word
 *  ---
 *  tutu
 *  PtextDeP1
 *  pdf
 *  ---
 *  imp r2
 *  toto
 *  PtextDeW1image1
 *  pdf
 *  ---
 *  tutu
 *  PtextDeP1
 *  pdf
 *  ---
 *  == recopie == 
 *  imp r1
 *  toto
 *  PtextDeW1image1
 *  word
 *  ---
 *  tutu
 *  PtextDeP1
 *  pdf
 *  ---
 *  imp r2
 *  toto
 *  PtextDeW1image1
 *  word
 *  ---
 *  tutu
 *  PtextDeP1
 *  pdf
 *  ---
 */
