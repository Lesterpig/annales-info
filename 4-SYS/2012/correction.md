Correction Système 2012
=======================

Question 1.1
------------

a- Les applications souhaitant réagir à des signaux systèmes doivent enregistrer des "handlers".
Certains signaux spéciaux disposent cependant de handlers par défaut (SIGTERM par exemple).

b- Un processus en arrière plan ne prend pas la main sur une interface graphique ou une console.
Il est possible de changer le plan via des signaux ou commandes.

c- L'accès aux fichiers proposés par les tubes anonymes ne peut être partagé que par fork.

d- Cette formule fait référence aux accès par blocs et s'applique en particulier aux disques durs, où l'accès aux données n'est (en général) pas séquentiel.

e- Les déroutements apparaissent lors du traitement des interruptions : le code d'une application en cours est momentannément suspendu,
un code traitant est exécuté, puis l'application reprend son fonctionnement normal (ou meurt).

f- Voir code d'exemple en C

```c
int pid_fils = fork();
if (pid_fils == 0) {
  pid_pere = getppid();
}
```

g- Il faut mettre à jour la table des pages du processus qui a "perdu" une page (bit de validité à zéro),
copier le contenu de la page à remplacer dans le disque (si modifiée), et remplacer le contenu de la page par les nouvelles données

h- Le DMA permet de soulager le CPU des instructions mémoires consécutives : il traite directement avec le contrôleur de disque pour récupérer les données en mémoire,
puis notifie le CPU via une interuptions quand le transfert est terminé.

i- Un processus peut contenir plusieurs thread, chaque thread partageant le même espace mémoire.
Un thread est donc une version "allégée" d'un processus.

j- Grâce à l'utilisation d'un sémaphore, bloquant le consommateur lorsque le pipe est vide.
On peut également utiliser des variables partagées.

Question 2.1
------------

```c
#include <unistd.h>
#include <stdio.h>

int main() {

  int i = 0;
  int j = 0;

  for (i = 1; i < 6; i++) {
    int pid = fork();

    if (pid == 0) {
      if (i == 1) {
        for (j = 1; j < 3; j++) {
          int pid2 = fork();
          if (pid2 == 0) {
            break;
          }
        }
      }
      break;
    }

  }

  printf("%d . %d\n", i == 6 ? 0 : i, j == 3 ? 0 : j);
  return 0;
}
```

Question 3.1
------------

1. On a 8 défauts en LRU

```
LOAD : défaut mémoire
OK   : pas de défaut mémoire

0 : LOAD (0)
7 : LOAD (0/7)
2 : LOAD (0/7/2)
7 : OK   (0/2/7)
5 : LOAD (0/2/7/5)
8 : LOAD (2/7/5/8)
9 : LOAD (7/5/8/9)
2 : LOAD (5/8/9/2)
4 : LOAD (8/9/2/4)
2 : OK   (8/9/4/2)
```

2. On a également 8 défauts en FIFO

```
0 : LOAD (0)
7 : LOAD (0/7)
2 : LOAD (0/7/2)
7 : OK   (0/7/2)
5 : LOAD (0/7/2/5)
8 : LOAD (7/2/5/8)
9 : LOAD (2/5/8/9)
2 : OK   (2/5/8/9)
4 : LOAD (5/8/9/4)
2 : LOAD (8/9/4/2)
```

3. On remarque que les pages 2 et 7 sont les seules à être appelées plusieurs fois.
La solution suivante propose 7 défauts, on ne pourra pas faire mieux.

```
0 : LOAD (0)
7 : LOAD (0/7)
2 : LOAD (0/7/2)
7 : OK   (0/7/2)
5 : LOAD (0/7/2/5)
8 : LOAD (7/2/5/8)
9 : LOAD (2/5/8/9)
2 : OK   (5/2/8/9)
4 : LOAD (2/8/9/4)
2 : OK   (8/2/9/4)
```

Question 4.1
------------

Adresse virtuelle :
- Déplacement sur log2(4Ko) = 12 bits
- Numéro de page virtuelle sur (32-12) = 20 bits

Un processus a donc un espace virtuel total de 2^20 * 4Ko = 2^32 = 4Go

Question 4.2
------------

Probablement pour éviter les défauts de page, ou accélérer les accès mémoires du système (une traduction de moins !).

Question 4.3
------------

En question 4.1 on a vu que le numéro de page virtuelle était stocké sur 20 bits.
On a donc 2^20 lignes dans la table des pages.

Chaque ligne faisant 4 octets on a donc une table pesant 4Mo par processus.

Question 4.4
------------

```
0000 0000 10 00 0000 0010 1110 1111 0101
   Livre    |    Index   | Déplacement
Livre : 10 => 2
Index : 10 => 2

L'adresse virtuelle 802ef5 devient donc 2ccf4ef5

On a de la même façon :

402aaf -> 34ceaaf
5003   -> 34fe003
403345 -> 64a345
```

Question 4.5
------------

On suppose que la référence de la table des livre vers un livre pèse 4 octets.
La table des livres pèse donc 1024*4 = 4Ko

De la même façon, un livre pèse 4Ko et peut adresser 1024*4Ko = 4Mo de mémoire

1. Pour 100Ko et 1Mo de mémoire, on aura besoin d'un seul livre, soit 8Ko de mémoire
2. Pour 10Mo de mémoire, on aura besoin de 3 livres, soit 16Ko de mémoire

Question 5.1
------------

```
A = 2, B = 6
B = 5, A = 6
A = 2, B = 5
B = 5, A = 2
A = 2, B = 2
B = 2, A = 2
A = 5, B = 5
B = 5, A = 5
```

Question 6.1
------------

Variables partagées :

```c
init(A,0)
init(B,0)
init(C,1)
```

P1

```c
while (true) {
  P(C);
  printf("A");
  V(A);
}
```

P2

```c
while (true) {
  P(A);
  printf("B");
  V(B);
}
```

P3

```c
while (true) {
  P(B);
  printf("C");
  V(C);
}
```

Question 6.2
------------

Variables partagées :

```c
init(A,0)
init(B,0)
init(C,0)
```

P1

```c
int i = 0;
while (true) {
  printf("A");
  if (i++ == 0) {
    V(B);
  } else {
    V(C);
  }
  i %= 2;
  P(A);
}
```

P2

```c
while (true) {
  P(B);
  printf("B");
  V(A);
}
```

P3

```c
while (true) {
  P(C);
  printf("C");
  V(A);
}
```

Question 6.3
------------

Variables partagées :

```c
init(A,0)
init(B,0)
init(C,0)
```

P1

```c
int i = 0;
while (true) {
  printf("A");
  if (i++ == 0) {
    V(B);
    P(A);
    V(C);
    P(A);
  } else {
    V(C);
    P(A);
    V(B);
    P(A);
  }
  i %= 2;
}
```

P2

```c
while (true) {
  P(B);
  printf("B");
  V(A);
}
```

P3

```c
while (true) {
  P(C);
  printf("C");
  V(A);
}
```

Question 7.1
------------


Question 7.2
------------
